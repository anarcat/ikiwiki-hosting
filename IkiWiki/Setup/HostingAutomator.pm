#!/usr/bin/perl
package IkiWiki::Setup::HostingAutomator;

use warnings;
use strict;
use YAML;

sub import {
	my $class=shift;
	my $setup=shift;

	# Expand embedded $ENV{foo}, escaping single quotes for YAML.
	$setup=~s{\$ENV{(.*?)}}{
		my $v=$ENV{$1};
		$v=~s/\n//g; # should never have newlines
		$v=~s/'/''/g;
		$v;
	}eg;

	require IkiWiki::Setup::Automator;
	IkiWiki::Setup::Automator->import(%{Load($setup)});
}

1
