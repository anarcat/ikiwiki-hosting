Home directory perms currently allow global reading of most files. Lock
down to 700?

> I think I agree with 0700. Let's do that and see what breaks. --liw

> The first thing to break will be apache, so this will need to be done
> with care.. Probably safest to only lock down `source`, `source.git`,
> `~/.git`, `~/ikiwiki.setup`.
>
> Hmm, the git daemon and gitweb run unprivilidged and need access to
> `source/.ikiwiki` (gitweb config files could move elsewhere) and
> `source.git` .. --[[Joey]]

>> Would 0711 on the home dir work? Ah, but everyone will have
>> files with well-known names. --[[liw]]

---

Things that need to be readable by other than the site user:

* `~/apache.conf` (owned by root:root, and should not contain sensative
  stuff; the same info is filled into /etc/apache2 config files anyway)

Things that are already appropriatly locked-down:

* logs
* `~/.ssh/authorized_keys`
* `~/.ikisite-nonce` (transient file)
* backups
* `~/ikiwiki.setup`
* `~/.gitconfig`, `~/.gitignore`
* `~/.git/`
* `~/tmp/`
* `~/public_html` (750; group www-data)
* `~/source/.ikiwiki` (other than the gitweb config files randomly
  located in here)
* `~/source/.ikiwiki/gitweb*` (with suexec changes, gitweb runs as user)
* `~/source` as a whole
* `~/source.git` (when branchability is disabled)
* `~/customersite/`
* `~/apache/` (750; group www-data)

[[done]]
