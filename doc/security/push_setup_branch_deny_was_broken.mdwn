Ikisite created a broken symlink for the git update hook, so pushes to the
setup branch were not denied. This could possibly lead to unauthorized code
execution, although it would have to be paired with a site branch or
backup/restore.

This is fixed, but any existing sites need to have their update hook manually
fixed to link to /usr/bin/iki-git-hook-update

[[done]] --[[Joey]] 
